Thufif Hawat
============

Back up or restore default configuration for miArcade batocera.


## Install

```
wget -O - https://gitlab.com/miarcade/ThufirHawat/-/raw/main/install.sh | bash
```

## Usage

```
usage: thufirhawat.py [-h] [-b] [-r] [-s] [-u] [uri]

Thufir Hawat: Restore config to default.

positional arguments:
  uri            URL of git repository or destiny SFTP.

optional arguments:
  -h, --help     show this help message and exit
  -b, --backup
  -r, --restore
  -s, --sftp
  -u, --update
```

## TODO

- Reboot after update (`batocera-es-swissknife --restart`)? Message to the user?
- Option for update theme only or everything but theme (to avoid heavy data transfer).

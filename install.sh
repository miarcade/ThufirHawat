#!/bin/bash

# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail
IFS=$'\n\t'

# https://github.com/RenatGilmanov/shell-script-template/blob/master/getopts/template-commands.sh
# ------------------------------------------------------------------
# [WTFPL](http://www.wtfpl.net/)
# (ɔ)2022 <pacoqueen@gmail.com> Title
#                               Description
# ------------------------------------------------------------------

SUBJECT=some-unique-id
VERSION=0.1.0

# https://github.com/z017/shell-script-skeleton/blob/master/skeleton
#######################################
# help command
#######################################
function help_command() {
  cat <<END;

USAGE:
  $0 [options] <command>
OPTIONS:
  -h            Ayuda
  -v            Versión

COMMANDS:
  help          Muestra esta ayuda.
  version       Muestra la versión.
END
  exit 1
}

# --- Option processing --------------------------------------------
# if [ $# == 0 ] ; then
#     help_command
# fi

while getopts ":vh" optname
  do
    case "$optname" in
      "v")
        echo "Versión $VERSION"
        exit 0;
        ;;
      "h")
        help_command
        exit 0;
        ;;
      "?")
        echo "Unknown option $OPTARG"
        exit 0;
        ;;
      ":")
        echo "No argument value for option $OPTARG"
        exit 0;
        ;;
      *)
        echo "Unknown error while processing options"
        exit 0;
        ;;
    esac
  done
shift $(($OPTIND - 1))

# -----------------------------------------------------------------

LOCK_FILE=/tmp/${SUBJECT}.lock

if [ -f "$LOCK_FILE" ]; then
    echo "Script ya ejecutándose."
    exit
fi

trap "rm -f $LOCK_FILE" EXIT
touch $LOCK_FILE 

# -----------------------------------------------------------------

if [ $# -gt 0 ] && [ "$1" = "help" ]; then
    help_command
elif [ $# -gt 0 ] && [ "$1" = "version" ]; then
    echo "Versión $VERSION"
fi;

#param2=$2
# etc.

# -----------------------------------------------------------------
#  RUTINA PRINCIPAL DEL SCRIPT AQUÍ
# -----------------------------------------------------------------

mkdir -p /userdata/bin/thufirhawat
cd /userdata/bin/thufirhawat
curl -O https://gitlab.com/miarcade/ThufirHawat/-/raw/main/thufirhawat.py
curl -O https://gitlab.com/miarcade/ThufirHawat/-/raw/main/thufirhawat_test.py
curl -O https://gitlab.com/miarcade/ThufirHawat/-/raw/main/params.py
curl -O https://gitlab.com/miarcade/ThufirHawat/-/raw/main/LICENSE
curl -O https://gitlab.com/miarcade/ThufirHawat/-/raw/main/README.md
chmod +x /userdata/bin/thufirhawat/thufirhawat.py

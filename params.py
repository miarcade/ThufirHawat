#!/usr/bin/env python
# coding: utf-8

"""
Configure here default parameters for git repository, SFTP destination for
backups and paths in batocera.
"""

from __future__ import print_function
import os

CONFIG_REPO = "https://gitlab.com/miarcade/userdata.git"
CONFIG_REPO_TAR = "https://gitlab.com/miarcade/userdata/-/archive/main/userdata-main.tar"
CONFIG_REPO_TAR_256 = "https://gitlab.com/miarcade/userdata/-/archive/premium/userdata-main.tar"
CONFIG_REPO_TAR_128= "https://gitlab.com/miarcade/userdata/-/archive/advance/userdata-main.tar"
CONFIG_REPO_TAR_64 = "https://gitlab.com/miarcade/userdata/-/archive/retro/userdata-main.tar"
LOCAL_BATOCERA_PATH = "/userdata/"
BASE_DEV_PATH = "/home/queen/Q-INN/BitBlue/src/miarcade.com/Alcuino [rpi4-batocera] (may 2022)/configs/userdata/"
USER_SFTP_DEV="queen"
HOST_SFTP_DEV="192.168.1.201"
CONFIG_SFTP_DEV = "{}@{}:{}".format(USER_SFTP_DEV, HOST_SFTP_DEV, BASE_DEV_PATH)
# ¿El saves? ¿Restaurar partidas guardadas y máximas puntuaciones? Mmmm... no sé.
DIRS = ('saves', 'splash', 'system', os.path.join('roms', 'miarcade'))
CONFIG_PATHS = [os.path.join(LOCAL_BATOCERA_PATH, dir) for dir in DIRS]
USER_SFTP_BATOCERA = "root"
HOST_SFTP_BATOCERA = "192.168.1.13"
CONFIG_SFTP_BATOCERA = ["{}@{}:{}".format(USER_SFTP_BATOCERA,
    HOST_SFTP_BATOCERA,
    os.path.join(LOCAL_BATOCERA_PATH, remote_path)) for remote_path in DIRS]
CONFIG_THEME_REPO = "https://gitlab.com/miarcade/miarcade-bt.git"
CONFIG_THEME_REPO_TAR = "https://gitlab.com/miarcade/miarcade-bt/-/archive/main/miarcade-bt-main.tar"
CONFIG_THEME_REPO_TAR_256 = "https://gitlab.com/miarcade/miarcade-bt/-/archive/premium/miarcade-bt-main.tar"
CONFIG_THEME_REPO_TAR_128 = "https://gitlab.com/miarcade/miarcade-bt/-/archive/advance/miarcade-bt-main.tar"
CONFIG_THEME_REPO_TAR_64 = "https://gitlab.com/miarcade/miarcade-bt/-/archive/retro/miarcade-bt-main.tar"
LOCAL_BATOCERA_THEME_PATH = "/userdata/themes/miarcade-bt"
SCRIPT_REPO="https://gitlab.com/miarcade/ThufirHawat.git"
SCRIPT_REPO_TAR="https://gitlab.com/miarcade/ThufirHawat/-/archive/main/ThufirHawat-main.tar"
SCRIPT_PATH="/userdata/bin/thufirhawat"
CONFIG_MANUAL_REPO_RAW="https://gitlab.com/miarcade/miarcade-bt/-/raw/main/manual/notice.pdf"
LOCAL_BATOCERA_MANUAL="/usr/share/batocera/doc/notice.pdf"
LOCAL_BATOCERA_CONF="/userdata/system/batocera.conf"
DELETEFILES=('README.md', 'LICENSE', '.gitignore',
             'roms/miarcade/túnel remoto.sh', 'roms/miarcade/test.py',
             'roms/miarcade/restaurar configuración.py',
             'roms/miarcade/actualizar y reiniciar (~1 min).sh',
             'roms/miarcade/auto actualizar.py',
             'roms/miarcade/auto actualizar.log',
             'roms/miarcade/restaurar configuración.log')

def print_vars():
    print("CONFIG_REPO", CONFIG_REPO)
    print("CONFIG_SFTP_DEV", CONFIG_SFTP_DEV)
    print("DIRS", DIRS)
    print("CONFIG_PATHS", CONFIG_PATHS)
    print("CONFIG_SFTP_BATOCERA", CONFIG_SFTP_BATOCERA)

if __name__ == "__main__":
    print_vars()

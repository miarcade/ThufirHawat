#!/usr/bin/env python
# coding: utf-8

"""thufirhawat.py: Manages default configs for miArcade customization."""

# TODO: De alguna forma debería guardar un backup del script por si en caso de
#       autoactualización desde una versión inestable se rompiera, poder hacer
#       _backtrack_ a la versión funcional y desde ahí volver a actualizar.
# TODO: La opción de restaurar configuración de fábrica se trae en realidad
#       la última versión (ver `roms/miarcade`). Añadir una opción para
#       traerse el tag "Release Candidate" o algo así.

from __future__ import print_function

__author__ = "Paco Queen"
__copyright__ = "Copyleft 2022, ℝ𝔹𝟜"
__license__ = "GPL"
__version__ = "0.99.1"
__email__ = "bogado@qinn.es"

import sys, os, stat
import subprocess
import argparse
import logging

from params import CONFIG_REPO, CONFIG_SFTP_DEV, CONFIG_PATHS
from params import LOCAL_BATOCERA_PATH, CONFIG_SFTP_BATOCERA, BASE_DEV_PATH
from params import LOCAL_BATOCERA_THEME_PATH, CONFIG_REPO_TAR
from params import CONFIG_REPO_TAR_256, CONFIG_REPO_TAR_128, CONFIG_REPO_TAR_64
from params import CONFIG_THEME_REPO_TAR, CONFIG_THEME_REPO_TAR_256
from params import CONFIG_THEME_REPO_TAR_128, CONFIG_THEME_REPO_TAR_64
from params import SCRIPT_REPO_TAR, SCRIPT_PATH
from params import CONFIG_MANUAL_REPO_RAW, LOCAL_BATOCERA_MANUAL
from params import LOCAL_BATOCERA_CONF, DELETEFILES

class ThufirHawat:
    def __init__(self, uri):
        """ Constructor. """
        logging.basicConfig(filename=sys.argv[0].replace(".py", ".log"),
                            level=logging.DEBUG,
                            format='%(asctime)s [%(levelname)s] %(message)s')
        self.logger = logging.getLogger(__name__)
        if uri:
            self.uri = uri
        else:
            self.uri = None     # It will be determined later.
        self.theme_uri = CONFIG_THEME_REPO_TAR
        self.manual_uri = CONFIG_MANUAL_REPO_RAW

    def do_backup(self, orig=CONFIG_PATHS):
        """
        Transfer all files in local configuration paths via SFTP to a ssh
        address in order to be uploaded to gitlab later.
        (There's no easy way to install git on batocera)
        """
        res = True
        self.logger.debug("backup from {} to {}...".format(orig, self.uri))
        for dirc in orig:
            dest = self.uri
            self.logger.debug("  >> backup {} to {}".format(dirc, dest))
            # cmd = ['bash', '-i', '-c',
            cmd = ['/usr/bin/scp', '-r', dirc, dest]
            cmd = '/usr/bin/scp -r {} {}'.format(dirc, dest)
            self.logger.debug(cmd)
            # TODO: Threading, sanitize params and so on.
            # os.system(cmd)
            run = subprocess.Popen(cmd, shell=True)
            res = run.wait() and res
            self.logger.info("{} [DONE]".format(cmd))
        return res

    def do_restore(self, dest=LOCAL_BATOCERA_PATH, restore_theme=True,
                   restore_manual=True, restore_saves=False,
                   version=None):
        """
        Download config from git repository and replaces local files with
        downloaded ones.
        One of the steps is save local wifi password and overwrite the default
        one with this after de restore is finished.
        """
        wificonfig = self.get_wifi_config()
        if not self.uri:
            if version in (256, "256", "premium"):
                self.uri = CONFIG_REPO_TAR_256
            elif version in (128, "128", "advance"):
                self.uri = CONFIG_REPO_TAR_128
            elif version in (64, "64", "retro"):
                self.uri = CONFIG_REPO_TAR_64
            else:
                self.uri = CONFIG_REPO_TAR
        self.logger.debug("Ver. {}".format(version))
        self.logger.debug("restore from {} into {}...".format(self.uri, dest))
        tempath = download(self.uri)
        if not restore_saves:
            overwrite(tempath, dest, exclude_dir="saves")
        else:
            overwrite(tempath, dest)
        # QUICK FIX: WORKAROUND: Elimino a mano los ficheros antiguos. Ya
        # idearé algún sistema para hacer un `rsync --delete` o algo así.
        for deletename in DELETEFILES:
            try:
                os.unlink(os.path.join(dest, deletename))
            except OSError:
                pass
        self.overwrite_wifi_config(wificonfig)
        if restore_theme:
            self.do_restore_theme(version = version)
        if restore_manual:
            self.do_restore_manual()
        # DONE: Y después corregir los permisos de /userdata/system porque
        # si no, el dropbear (servidor SSH ligero) no deja entrar con
        # clave pública.
        # chmod 700 /userdata/system/
        # chmod 700 /userdata/system/.ssh/
        # chmod 700 /userdata/system/ssh/
        # chmod 600 /userdata/system/ssh/authorized_keys
        # chmod 600 /userdata/system/.ssh/authorized_keys
        perms = ((stat.S_IRWXU, "/userdata/system/"),
                 (stat.S_IRWXU, "/userdata/system/.ssh/"),
                 (stat.S_IRWXU, "/userdata/system/ssh/"),
                 (stat.S_IREAD|stat.S_IWRITE, "/userdata/system/ssh/authorized_keys"),
                 (stat.S_IREAD|stat.S_IWRITE, "/userdata/system/.ssh/authorized_keys"))
        for permisos, fichero in perms:
            os.chmod(fichero, permisos)

    def do_restore_theme(self, dest=LOCAL_BATOCERA_THEME_PATH, version=None):
        """
        Restore miarcade-bt theme from git in `dest` path.
        """
        if version and version in (256, "256", "premium"):
            self.theme_uri = CONFIG_THEME_REPO_TAR_256
        elif version and version in (128, "128", "advance"):
            self.theme_uri = CONFIG_THEME_REPO_TAR_128
        elif version and version in (64, "64", "retro"):
            self.theme_uri = CONFIG_THEME_REPO_TAR_64
        self.logger.debug("restore theme from {} into {}...".format(
            self.theme_uri, dest))
        tempath = download(self.theme_uri)
        overwrite(tempath, LOCAL_BATOCERA_THEME_PATH)

    def do_restore_manual(self, dest=LOCAL_BATOCERA_MANUAL):
        """
        Overwrites batocera's PDF manual with a custom one.
        """
        orig = self.manual_uri
        self.logger.debug("restore manual from {} to {}...".format(orig, dest))
        tempath = download(orig)
        overwrite(tempath, dest)
        cmd = ["/usr/bin/batocera-save-overlay"]
        self.logger.debug(cmd)
        run = subprocess.Popen(cmd)
        res = run.wait()
        self.logger.info("{} [DONE]".format(cmd))
        return res

    def do_sftp(self, orig=CONFIG_SFTP_BATOCERA, dest=BASE_DEV_PATH):
        """
        Create a copy of config files of test arcade machine to dev
        computer. It runs from dev computer itself and SSH keys has to be
        installed on batocera (`ssh-copy-id`).
        """
        # TODO: Check version and change local git branch according to it.
        self.logger.debug("sftp from {} into {}...".format(orig, dest))
        res = True
        for fullremotepath in orig:
            # El soporte de miarcade va en forma de scripts .py|.sh como ROM
            # ejecutable. Va en un subdirectorio, hay que modificar
            # ligeramente la ruta de destino.
            if "roms" in fullremotepath:
                _dest = dest + "roms/"
            else:
                _dest = dest
            cmd = ["/usr/bin/scp", "-r", fullremotepath, _dest]
            self.logger.debug(cmd)
            run = subprocess.Popen(cmd)
            res = run.wait() and res
            self.logger.info("{} [DONE]".format(cmd))
        return res

    def self_update(self):
        """
        Download latest version from git repository and overwrites this script.
        """
        self.logger.debug("Autoupdating...")
        tempath = download(SCRIPT_REPO_TAR)
        self.logger.debug("{}  downloaded on {}.".format(SCRIPT_REPO_TAR, tempath))
        overwrite(tempath, SCRIPT_PATH)
        self.logger.debug("Update finished.")
        sys.exit(0)

    def get_wifi_config(self):
        """
        Find local current wifi password and returns it.
        """
        res = {}
        with open(LOCAL_BATOCERA_CONF, "r") as batoconf:
            for line in batoconf.readlines():
                if line.startswith("wifi."):
                    key, value = line.split("=")
                    # Key header will be added later. Only pure key.
                    key = key.replace("wifi.", "")
                    res[key] = value
        return res

    def overwrite_wifi_config(self, wificonfig):
        """
        Open batocera.conf and replace default wifi config with received one.
        Returns True if successful.
        """
        res = False
        with open(LOCAL_BATOCERA_CONF, "r") as batoconf:
            lines = batoconf.readlines()
        lines = [l for l in lines if not l.startswith("wifi.")]
        for key in wificonfig.keys():
            value = wificonfig[key]
            lines.append("wifi.{}={}\n".format(key, value))
        with open(LOCAL_BATOCERA_CONF, "w") as batoconf:
            batoconf.writelines(lines)
        return res


def download(uri):
    """
    Download tar archive to temp path and returns it.
    """
    # tempath = "/tmp/userdata-main.tar.bz2"
    # tempath = "/tmp/miarcade-bt-main.tar.bz2"
    # tempath = "/tmp/ThufirHawat-main.tar.bz2"
    fname = uri.split("/")[-1]
    tempath = os.path.join("/tmp", fname)
    cmd = ["wget", uri, "--quiet", "-O", tempath]
    print("Downloading {}...".format(uri))
    print(" ".join(cmd))
    run = subprocess.Popen(cmd)
    run.wait()
    return tempath

def overwrite(opath, dpath, exclude_dir=None):
    """
    Decompress `opath` and move files to `dpath` overwriting files if exists.
    """
    # TODO: It does not remove old files that aren't in tar update (been
    # deleted on repo).
    print("Overwriting {} with {}...".format(dpath, opath))
    run = subprocess.Popen(["mkdir", "-p", dpath])
    run.wait()
    if exclude_dir:
        print("Excluding {}...".format(exclude_dir))
    if opath.endswith(".tar"):
        cmd = ["tar", "xvf", opath, "--strip-components=1", "-C", dpath]
        if exclude_dir:
            cmd.append("--exclude=*/{}/*".format(exclude_dir))
    else:
        # cmd = ["cp", "-a", "-f", opath, dpath]
        cmd = ["rsync", "-av", "--progress", opath, dpath]
        if exclude_dir:
            cmd.append += ["--exclude", "*/{}".format(exclude_dir)]
    print(" ".join(cmd))
    run = subprocess.Popen(cmd)
    res = run.wait()
    return res


def main():
    """ Rutina principal. """
    logging.basicConfig(filename=sys.argv[0].replace(".py", ".log"),
                        level=logging.DEBUG,
                        format='%(asctime)s [%(levelname)s] %(message)s')
    parser = argparse.ArgumentParser(
            description="Thufir Hawat: Restore config to default.")
    parser.add_argument("-b", "--backup", action='store_true', default=False,
            help="Backup on live machine.")
    parser.add_argument("-r", "--restore", action='store_true', default=False,
            help="Restore from git repository on live machine.")
    parser.add_argument("-s", "--sftp", action='store_true', default=True,
            help="Retrieve config from machine to dev environment via SFTP.")
    parser.add_argument("-u", "--update", action='store_true', default=False,
            help="Autoupdate this script.")
    parser.add_argument("-t", "--theme", action='store_true', default=False,
            help="Update miArcade theme only.")
    parser.add_argument("-m", "--manual", action='store_true', default=False,
            help="Update miArcade manual only.")
    parser.add_argument("-g", "--savegames", action='store_true', default=False,
            help="Overwrite local savesfiles with remote ones.")
    parser.add_argument("version", help="Version {256|128|64}",
                        default='premium', nargs='?')
    parser.add_argument("uri", help="URL del repositorio o SFTP de destino.",
                        default=None, nargs='?')
    args = parser.parse_args()
    # print(args.version)
    # sys.exit(10)
    if not args.version or args.version.lower() not in (
            '256', '128', '64', 'premium', 'advance', 'retro'):
        if not args.update:
            print("Invalid version")
            sys.exit(1)
    else:
        args.version = args.version.lower()
    logging.debug("> version: {}".format(args.version))
    logging.debug("> backup:  {}".format(args.backup))
    logging.debug("> restore: {}".format(args.restore))
    logging.debug("> sftp:    {}".format(args.sftp))
    logging.debug("> update:  {}".format(args.update))
    logging.debug("> theme:   {}".format(args.theme))
    logging.debug("> manual:  {}".format(args.manual))
    logging.debug("> uri:     {}".format(args.uri))
    logging.debug("> saves:   {}".format(args.savegames))
    if args.update:
        ThufirHawat(args.uri).self_update()
    elif args.backup:
        ThufirHawat(args.uri).do_backup()
    elif args.restore:
        ThufirHawat(args.uri).do_restore(restore_saves = args.savegames,
                                         version = args.version)
    elif args.theme:
        ThufirHawat(args.uri).do_restore_theme(version = args.version)
    elif args.manual:
        ThufirHawat(args.uri).do_restore_manual()
    elif args.sftp:
        # TODO: Esta opción siempre se activa y no debería ser así. Si acaso, por defecto, la de restore.
        ThufirHawat(args.uri).do_sftp()
    else:
        parser.print_help()
    sys.exit(0)

if __name__ == "__main__":
    main()

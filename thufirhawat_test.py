#!/usr/bin/env python
# coding: utf-8

from __future__ import print_function

import os
import unittest

from params import CONFIG_REPO, LOCAL_BATOCERA_PATH, BASE_DEV_PATH
from params import USER_SFTP_DEV, HOST_SFTP_DEV, CONFIG_SFTP_DEV, DIRS, CONFIG_PATHS
from thufirhawat import ThufirHawat

class TestParams(unittest.TestCase):
    """ Testing parameters coherence. """
    def setUp(self):
        self.mentat = ThufirHawat(CONFIG_REPO)

    def test_uri(self):
        self.assertTrue(self.mentat.uri.startswith("https://")
                        or "gitlab" in self.mentat.uri.lower()
                        or "@" in self.mentat.uri)

class TestConfig(unittest.TestCase):
    """ Testing default configuration. """

    def test_sftp(self):
        self.assertTrue("@" in CONFIG_SFTP_DEV and ":" in CONFIG_SFTP_DEV)
    def test_git(self):
        self.assertTrue("gitlab" in CONFIG_REPO)

class TestBackup(unittest.TestCase):
    """ Simple test for backup. """

    def setUp(self):
        # parámetros y la configuración y comprobaremos si se ha copiado.
        # XXX: HARCODED
        CONFIG_REPO = 'queen@sulaco:/tmp'
        self.mentat = ThufirHawat(CONFIG_REPO)

    def test_backup(self):
        filedest = 'hosts'
        orig = [os.path.join('/etc', filedest)]
        self.mentat.do_backup(orig)
        self.assertTrue(os.path.exists(os.path.join('/tmp', filedest)))
        try:
            os.unlink(os.path.join('/tmp', filedest))
        except OSError:
            pass    # No se llegó a copiar.

if __name__ == '__main__':
    unittest.main()
